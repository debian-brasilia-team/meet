include .env
export

REGISTRY := debianbsb/meet

build:
	podman build \
		-t $(REGISTRY):$(VERSION) .

run:
	podman-compose up --build

run-web:
	podman run -it --rm \
		--network host \
		-v `pwd`:/app \
		--env-file .env $(REGISTRY):$(VERSION)

lint:
	podman run -it --rm \
		--entrypoint=/usr/local/bin/python \
		-v `pwd`:/app \
		$(REGISTRY):$(VERSION) \
		-m flake8 --max-line-length=999

shell:
	podman run -it --rm \
		--entrypoint=/usr/local/bin/python \
		-v `pwd`:/app \
		--network host \
		--entrypoint sh \
		$(REGISTRY):$(VERSION)

freeze:
	podman run -it --rm \
		--entrypoint=/usr/local/bin/python \
		-v `pwd`:/app \
		--network host \
		--entrypoint pip \
		$(REGISTRY):$(VERSION) \
		freeze > requirements.txt

login:
	podman login registry.salsa.debian.org

build-push-dockerhub:
	docker buildx build --push --platform linux/arm64/v8,linux/amd64 --tag $(REGISTRY):$(VERSION) .

push:
	podman push $(REGISTRY):$(VERSION)
	podman tag $(REGISTRY):$(VERSION) $(REGISTRY):latest
	podman push $(REGISTRY):latest
