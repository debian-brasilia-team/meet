from fastapi import APIRouter
from .endpoints import index
from .endpoints import delete_cookies

web_router = APIRouter(include_in_schema=False)

web_router.include_router(index.router, tags=['index'])
web_router.include_router(delete_cookies.router, tags=['delete_cookies'])
