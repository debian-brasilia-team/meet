from fastapi import APIRouter
from fastapi.responses import RedirectResponse

router = APIRouter()


@router.get("/delete_cookies")
async def delete_cookies():
    response = RedirectResponse('/')
    response.delete_cookie('name')
    response.delete_cookie('email')
    response.delete_cookie('cookies_accepted')

    return response
