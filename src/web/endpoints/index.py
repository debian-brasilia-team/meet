import io
import os
from datetime import datetime, timezone, timedelta
import gzip
import uuid
import json
from fastapi import APIRouter, Depends, Request, Form
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
import starlette.status as status
from src.cookies import get_cookies
from src.minio import client as minio_client
from src.constants import BUCKET_NAME
from minio.error import S3Error

router = APIRouter()
templates = Jinja2Templates(directory='src/web/templates')


@router.get('/', response_class=HTMLResponse)
async def index_get(
    request: Request,
    cookies: str = Depends(get_cookies)
):

    response = templates.TemplateResponse(
        'index.html',
        {
            'request': request,
            'name': cookies['name'] if cookies['cookies_accepted'] else '',
            'email': cookies['email'] if cookies['cookies_accepted'] else ''
        }
    )

    return response


def get_path(dt_now: datetime):
    year = dt_now.year
    month = dt_now.month
    day = dt_now.day
    name = uuid.uuid4()

    path = f"{year}/{month}/{day}/{name}.json.gz"

    return path


@router.post("/")
async def index_post(
    request: Request,
    name: str = Form(''),
    email: str = Form(''),
    cookies: str = Depends(get_cookies)
):
    dt_now = datetime.now(timezone.utc)

    object_name = get_path(dt_now)
    created_at = dt_now.isoformat()

    payload = {
        'name': name,
        'email': email,
        'method': request.method,
        'url': str(request.url),
        'headers': dict(request.headers),
        'query_params': str(request.query_params),
        'path_params': str(request.path_params),
        'cookies': request.cookies,
        'client': str(request.client),
        'created_at': created_at
    }

    data = json.dumps(payload).encode('utf-8')
    compressed_data = gzip.compress(data)

    try:
        minio_client.put_object(BUCKET_NAME, object_name, io.BytesIO(compressed_data), len(compressed_data))

        print(f"Object '{object_name}' successfully uploaded to bucket '{BUCKET_NAME}'")
    except S3Error as e:
        print(f"Error: {e}")

    response = RedirectResponse(url=os.environ["MEETING_INSTANCE"], status_code=status.HTTP_303_SEE_OTHER)
    cookie_expires = dt_now + timedelta(days=360)

    if cookies['cookies_accepted']:
        response.set_cookie(key="name", value=name, expires=cookie_expires)
        response.set_cookie(key="email", value=email, expires=cookie_expires)

    return response
