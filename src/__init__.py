from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from prometheus_fastapi_instrumentator import Instrumentator
from fastapi.staticfiles import StaticFiles
from src.web import web_router
from os import getenv
import os
import sys
from src.minio import client as minio_client
from minio.error import S3Error
from dotenv import load_dotenv
from src.constants import BUCKET_NAME

LOG_LEVEL = getenv('LOG_LEVEL', 'INFO').upper()
VERSION = getenv('VERSION')
DEBUG = True if LOG_LEVEL == "DEBUG" else False


description = """
"""

application = FastAPI(
    docs_url=None,
    title='Debian Meet',
    description=description,
    debug=DEBUG,
    version=VERSION,
    contact={
        'name': 'Debian Brasilia Team Meet',
        'url': 'https://meet.debianbsb.org',
        'email': 'debianbsb@gmail.com',
    },
    license_info={
        'name': 'Apache 2.0',
        'url': 'https://www.apache.org/licenses/LICENSE-2.0.html',
    },
)


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
load_dotenv(os.path.join(BASE_DIR, ".env"))
sys.path.append(BASE_DIR)


def configure_minio():
    try:
        if not minio_client.bucket_exists(BUCKET_NAME):
            minio_client.make_bucket(BUCKET_NAME)
    except S3Error as e:
        print(f"Error: {e}")


def configure_routes():
    application.include_router(web_router)


def configure_instrumentation():
    Instrumentator(
        should_group_status_codes=False,
        should_ignore_untemplated=True,
        should_respect_env_var=False,
        should_instrument_requests_inprogress=True,
        excluded_handlers=['/metrics'],
        env_var_name='ENABLE_METRICS',
        inprogress_name='inprogress',
        inprogress_labels=True,
    ).instrument(application).expose(application, include_in_schema=False)


def configure_mount():
    application.mount(
        '/static',
        StaticFiles(
            directory='src/web/static'
        ),
        name='static'
    )


def configure_cors():
    origins = [
        'http://localhost:8080',
        'https://meet.debianbsb.org'
    ]

    application.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=['GET'],
        allow_headers=['*']
    )


def get_app() -> FastAPI:
    configure_routes()
    configure_mount()
    configure_instrumentation()
    configure_cors()
    configure_minio()

    return application


app = get_app()
