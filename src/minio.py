from os import getenv
from minio import Minio
from src.utils import str_to_bool

client = Minio(
    getenv("MINIO_HOST"),
    access_key=getenv("MINIO_ACCESS_KEY") or getenv("MINIO_ROOT_USER"),
    secret_key=getenv("MINIO_SECRET_KEY") or getenv("MINIO_ROOT_PASSWORD"),
    secure=str_to_bool(getenv("MINIO_SECURE", True))  # Set secure to False for HTTP connection
)
