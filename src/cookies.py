from fastapi import Cookie
from typing import Union


async def get_cookies(
    email: Union[str, None] = Cookie(default=''),
    name: Union[str, None] = Cookie(default=''),
    cookies_accepted: Union[str, None] = Cookie(default='')
):

    return {'name': name, 'email': email, 'cookies_accepted': cookies_accepted}
