# meet

Simple proxy/redirect meeting webserver that also collects metrics.

## Dependencies

- Podman
- Python >= 3
- Make

```bash
apt install python3-venv python3-pip python3-dev podman podman-compose
```

## Development

| Name   | Command       | Description                                                       |
|--------|---------------|-------------------------------------------------------------------|
| Build  | `make build`  | Build the Dockerfile into a Docker image.                         |
| Run    | `make run`    | Execute podman-compose and start web and minio services.          |
| Lint   | `make lint`   | Check lint flake8 lint policies defined at `.flake8`.             |
| Push   | `make push`   | Push Image to the docker registry.                                |
| Shell  | `make shell`  | Init a container but with shell attached for debug.               |
| Freeze | `make freeze` | Lock pip dependencies to `requirements.txt` file.                 |
| Clean  | `make clean`  | Clean cached folders and files.                                   |

- Web: [http://locahost:8080](http://locahost:8080)
- Bucket API: [http://localhost:9000](http://localhost:9000)
- Bucket Console: [http://localhost:9001](http://localhost:9001)

> Default username and password for Bucket Console (minio) is: `minioadmin`

## Deploy

```bash
cat > production.env << EOF
RELOAD=False
PORT=8080
LOG_LEVEL=INFO
MINIO_SECURE=True
MINIO_HOST=...
MINIO_ACCESS_KEY=...
MINIO_SECRET_KEY=...
EOF
```

```bash
podman login registry.salsa.debian.org

podman run -d \
    --name bugs-api \
    --restart unless-stopped \
    --network host \
    --env-file ./production.env \
    registry.salsa.debian.org/debian-brasilia-team/meet:latest
```

## Push to Docker Hub

> Currently we are pushing to Docker Hub because Salsa Container Registry does not support arm based images.

```bash
apt install docker.io

usermod -aG docker $USER
newgrp docker

docker login
```


#### Buildx setup

```bash
# Download build binary from GitHub
https://github.com/docker/buildx/releases

# Rename file to docker-buildx
mkdir $HOME/.docker/cli-plugins
mv docker-buildx $HOME/.docker/cli-plugins/
chmod +x $HOME/.docker/cli-plugins/docker-buildx

docker buildx install

# Create a build instance that supports arm64 and arm:
docker buildx create --name multi-arch \
  --platform "linux/arm64,linux/amd64,linux/arm/v7" \
  --driver "docker-container"

# Now we can update our default builder instance:
docker buildx use multi-arch
```

#### Build and Push

```
make build-push-dockerhub
```

## Setup and Run manually

```bash
# Create environment
python3 -m venv venv

# To activate the environment
source venv/bin/activate

python3 -m pip install -r requirements.txt

python3 main.py

# When you finish you can exit typing
deactivate

# Lock pip dependencies to requirements.txt file
python3 -m pip freeze > requirements.txt
```

## License

This app is licensed under the Apache 2 License. For more details, please see [LICENSE](https://salsa.debian.org/debian-brasilia-team/meeting/blob/main/LICENSE).
